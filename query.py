import sys
import openai

# Set important environment variables that can be set from the CI pipeline
api_key = sys.argv[1] # GPT API Key
apt_target = sys.argv[2] # APT group we're targeting
volume = sys.argv[3] # Number of results to return
format = sys.argv[4] # What format should the results be in? Suggested formats: json, yaml, csv

# Set the full query up for the GPT API call
full_query = "Return the top " + volume + " exploit command lines related to " + apt_target + " group in " + format + " format with references, ATT&CK mapping, and description"
# print(full_query)

# Set the API Key for OpenAI
openai.api_key = api_key

# Actually make the GPT API call
completion = openai.ChatCompletion.create(
  model="gpt-3.5-turbo",
  messages=[
    {"role": "user", "content": full_query}
  ],
  temperature=0,
  max_tokens=2026,
  top_p=1,
  frequency_penalty=0,
  presence_penalty=0
)

# Set the results to a variable
query_result = (completion.choices[0].message.content)

# Save to a file (include the APT name in the file)
with open(str(apt_target) + "." + str(format), "w") as file:
    file.write(query_result)
