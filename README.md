## Overview

Detectient is a tool to automatically generate threat detection signatures, using GPT3 (OpenAI) and GitLab CI/CD.

`NOTE` Detectient requires the ability to run GitLab CI/CD in your project, as well as a ChatGPT+/other AI provider subscription. 

## Components

There are 3 main components of Detectient: 
1. gitlab-ci.yml
   - This yml file is the main GitLab CI/CD configuration file. This file contains variables that can be changed to make Detectient create various types of threat detection signatures in various formats. 
2. query.py
    - This is the main Python script that queries GPT (OpenAI) and returns data in a format that is saved to the Detectient project

3. Rules folder
   - This folder contains detection results returned from GPT

## Getting Started 
1. Fork the Detectient project to your own namespace
2. If you don't want the initial set of rules that I've created, delete everything in the "Rules" folder. 
3. Set up required secrets. 
   1. OpenAI API Key: Generate an OpenAI API key, and save it in the project as a secure variable: go to Settings > CI/CD > Variables, and save it with the name `OPENAI_APIKEY`
   2. Project push token: In order to save results from GPT to Detectient, you need a project token with sufficient access. Go to Settings > Access Tokens, and set up a key with the following configuration:
      - Token Name: PUSH_KEY
      - Scopes: read_repository, write_repository
      - Role: Maintainer
    - Save the PUSH_KEY as a variable in Settings > CI/CD > Variables with the name `PUSH_KEY`

## Running Detectient
1. Open the .gitlab-ci.yml file and edit the desired settings in the `User Configurable Variables` section (what APT group to create detections for, etc). Upon saving the file, a pipeline will automatically be kicked off that queries GPT for results and saves them to the detection repository. 

2. Go to Build > Pipelines in the project to see the status of your most recent pipeline

3. Go to the Rules folder and view the contents of the most recent written file to review the threat detection signatures returned from GPT. 
